#include "GarrysMod/Lua/Interface.h"
#include "isacteng.h"
#include "isactri.h"
#include <filesystem>

#define LUA_PRINT(text) LUA->PushSpecial( GarrysMod::Lua::SPECIAL_GLOB ); \
LUA->GetField( -1, "print" ); \
LUA->PushString( text ); \
LUA->Call( 1, 0 ); \

#define ADD_ISACT_FUNC(func) LUA->PushCFunction( func ); LUA->SetField( -2, #func );

using namespace GarrysMod::Lua;
namespace fs = std::filesystem;

ISACTRenderInterface* g_pISACTRenderInterface;
ISACTInterface* g_pISACTInterface;

union GMIHANDLE
{
	IHANDLE isact;
	int gmod;
};

void PushIHANDLE( ILuaBase* LUA, IHANDLE iHandle )
{
	GMIHANDLE hnd = { iHandle };
	LUA->PushNumber( (double)( hnd.gmod ) );
}
IHANDLE GetIHANDLE( ILuaBase* LUA, int iStackPos )
{
	double num = LUA->GetNumber( iStackPos );
	
	GMIHANDLE hnd;
	hnd.gmod = (int)num;

	return hnd.isact;
}

void VectorToISACT( Vector& vecOrig, ISACTVector* pVecOut )
{
	pVecOut->fX = vecOrig.x;
	pVecOut->fY = vecOrig.z;
	pVecOut->fZ = vecOrig.y;
}
void ISACTToVector( ISACTVector& vecOrig, Vector* pVecOut )
{
	pVecOut->x = vecOrig.fX;
	pVecOut->y = vecOrig.fZ;
	pVecOut->z = vecOrig.fY;
}

void ShutdownISACT()
{
	if ( g_pISACTInterface )
	{
		g_pISACTInterface->ReleaseInterface();
		g_pISACTInterface = nullptr;
	}

	if ( g_pISACTRenderInterface )
	{
		g_pISACTRenderInterface->ReleaseInterface();
		g_pISACTRenderInterface = nullptr;
	}
}

bool InitISACT(ILuaBase* LUA)
{
	LUA_PRINT( "Retrieving render interface..." );
	g_pISACTRenderInterface = RetrieveISACTRenderInterface();
	if ( !g_pISACTRenderInterface )
	{
		LUA_PRINT( "Failed!" );
		ShutdownISACT();
		return false;
	}

	LUA_PRINT( "Retrieving ISACT interface..." );
	g_pISACTInterface = RetrieveISACTInterface( g_pISACTRenderInterface );
	if ( !g_pISACTInterface )
	{
		LUA_PRINT( "Failed!" );
		ShutdownISACT();
		return false;
	}

	g_pISACTInterface->SetDistanceFactor( 0.01905 );

	return true;
}

LUA_FUNCTION( GetAvailableVoices )
{
	long iCount = 0;
	g_pISACTInterface->GetAvailableVoiceCount(&iCount);
	LUA->PushNumber( iCount );

	return 1;
}

LUA_FUNCTION( LoadContentBank )
{
	if ( !LUA->CheckString( 1 ) )
	{
		return 0;
	}

	const char* pszBank = LUA->GetString( 1 );

	IHANDLE iBank;
	ISACTRETURN ir = g_pISACTInterface->LoadContentBank( &iBank, pszBank );
	if ( ir != ISACT_OK )
	{
		LUA->PushNumber( -1 );
		return 0;
	}

	PushIHANDLE( LUA, iBank );
	return 1;
}
LUA_FUNCTION( LoadSampleBank )
{
	if ( !LUA->CheckString( 1 ) )
	{
		return 0;
	}
	LUA->CheckType( 2, Type::Number );

	const char* pszBank = LUA->GetString( 1 );

	IHANDLE iBank;
	ISACTRETURN ir = g_pISACTInterface->LoadSampleBank( LUA->GetNumber(2), pszBank );
	if ( ir != ISACT_OK )
	{
		LUA->PushBool( false );
		return 0;
	}
	LUA->PushBool( true );
	return 1;
}
LUA_FUNCTION( GetContentHandle )
{
	LUA->CheckType( 1, Type::String );
	LUA->CheckType( 2, Type::Number );

	const char* pszContent = LUA->GetString( 1 );
	IHANDLE hBank = GetIHANDLE( LUA, 2 );

	IHANDLE iContent;

	ISACTRETURN ir = g_pISACTInterface->GetContentHandle( &iContent, pszContent, hBank );
	if ( ir != ISACT_OK )
	{
		LUA->PushNumber( -1 );
		return 0;
	}

	PushIHANDLE( LUA, iContent );
	return 1;
}

LUA_FUNCTION( CreateAudioPlayer )
{
	IHANDLE iPlayer;

	ISACTRETURN ir = g_pISACTInterface->CreateAudioPlayer( &iPlayer );
	if ( ir != ISACT_OK )
	{
		LUA->PushNumber( -1 );
		return 0;
	}

	PushIHANDLE( LUA, iPlayer );
	return 1;
}
LUA_FUNCTION( AttachPlayerPrimaryContent )
{
	LUA->CheckType( 1, Type::Number );
	LUA->CheckType( 2, Type::Number );

	IHANDLE hPlayer = GetIHANDLE( LUA, 1 );
	IHANDLE hContent = GetIHANDLE( LUA, 2 );

	ISACTRETURN ir = g_pISACTInterface->AttachPlayerPrimaryContent( hPlayer, hContent );
	if ( ir != ISACT_OK )
	{
		LUA_PRINT( "[ISACT] Failed to attach content!" );
		return 0;
	}

	return 1;
}
LUA_FUNCTION( PlayAudioPlayer )
{
	LUA->CheckType( 1, Type::Number );

	IHANDLE hPlayer = GetIHANDLE( LUA, 1 );

	long lLoopCount = 0;
	if ( LUA->IsType( 2, Type::Number ) )
		lLoopCount = (long)LUA->GetNumber( 2 );

	ISACTRETURN ir = g_pISACTInterface->PlayAudioPlayer( hPlayer, lLoopCount, NULL );
	if ( ir != ISACT_OK )
	{
		return 0;
	}
	
	return 1;
}
LUA_FUNCTION( SetAudioPlayerMode )
{
	LUA->CheckType( 1, Type::Number );
	LUA->CheckType( 2, Type::Number );

	IHANDLE hPlayer = GetIHANDLE( LUA, 1 );
	long lMode = (long)LUA->GetNumber( 2 );

	g_pISACTInterface->SetAudioPlayerMode( hPlayer, lMode );
	return 1;
}
LUA_FUNCTION( GetAudioPlayerMode )
{
	LUA->CheckType( 1, Type::Number );

	IHANDLE hPlayer = GetIHANDLE( LUA, 1 );

	unsigned long lMode;
	g_pISACTInterface->GetAudioPlayerMode( hPlayer, &lMode );

	LUA->PushNumber( (double)lMode );
	return 1;
}
LUA_FUNCTION( SetAudioPlayerPosition )
{
	LUA->CheckType( 1, Type::Number );
	LUA->CheckType( 2, Type::Vector );

	IHANDLE hPlayer = GetIHANDLE( LUA, 1 );
	Vector vecPos = LUA->GetVector( 2 );

	ISACTVector vecISACTVec;
	VectorToISACT( vecPos, &vecISACTVec );

	g_pISACTInterface->SetAudioPlayerPosition( hPlayer, vecISACTVec );

	return 1;
}
LUA_FUNCTION( GetAudioPlayerPosition )
{
	LUA->CheckType( 1, Type::Number );

	IHANDLE hPlayer = GetIHANDLE( LUA, 1 );

	ISACTVector vecPos;
	g_pISACTInterface->GetAudioPlayerPosition( hPlayer, &vecPos );

	Vector vecGMOD;
	ISACTToVector( vecPos, &vecGMOD );

	LUA->PushVector( vecGMOD );

	return 1;
}

LUA_FUNCTION( SetListenerPosition )
{
	LUA->CheckType( 1, Type::Vector );

	Vector vecPos = LUA->GetVector( 1 );

	ISACTVector vecISACTVec;
	VectorToISACT( vecPos, &vecISACTVec );

	g_pISACTInterface->SetListenerPosition( vecISACTVec );

	return 1;
}
LUA_FUNCTION( GetListenerPosition )
{
	ISACTVector vecPos;
	g_pISACTInterface->GetListenerPosition( &vecPos );

	Vector vecGMOD;
	ISACTToVector( vecPos, &vecGMOD );

	LUA->PushVector( vecGMOD );

	return 1;
}
LUA_FUNCTION( SetListenerOrientation )
{
	if ( LUA->IsType( 1, Type::Vector ) )
	{
		LUA->CheckType( 2, Type::Vector );

		Vector vecFwd = LUA->GetVector( 1 );
		Vector vecUp = LUA->GetVector( 2 );

		ISACTVector ISACTFwd;
		ISACTVector ISACTUp;

		VectorToISACT( vecFwd, &ISACTFwd );
		VectorToISACT( vecUp, &ISACTUp );

		g_pISACTInterface->SetListenerOrientation( ISACTFwd, ISACTUp );

		return 1;
	}
	else
	{
		LUA->CheckType( 1, Type::Number );
		LUA->CheckType( 2, Type::Number );
		LUA->CheckType( 3, Type::Number );

		double flAzimuth = LUA->GetNumber( 1 );
		double flHeight = LUA->GetNumber( 1 );
		double flRoll = LUA->GetNumber( 1 );

		g_pISACTInterface->SetListenerOrientation( flAzimuth, flHeight, flRoll );

		return 1;
	}
	return 0;

	
}


GMOD_MODULE_OPEN()
{
	LUA->PushSpecial( GarrysMod::Lua::SPECIAL_GLOB );
	LUA->GetField( -1, "print" );
	LUA->PushString( "ISACT module init!" );
	LUA->Call( 1, 0 );

	InitISACT(LUA);
	LUA_PRINT( "ISACT init finished!" );

	LUA->PushSpecial( SPECIAL_GLOB );
		LUA->CreateTable();
			ADD_ISACT_FUNC( GetAvailableVoices )
			ADD_ISACT_FUNC( LoadSampleBank )
			ADD_ISACT_FUNC( LoadContentBank )
			ADD_ISACT_FUNC( GetContentHandle )

			ADD_ISACT_FUNC( CreateAudioPlayer )
			ADD_ISACT_FUNC( AttachPlayerPrimaryContent )
			ADD_ISACT_FUNC( PlayAudioPlayer )
			ADD_ISACT_FUNC( SetAudioPlayerMode )
			ADD_ISACT_FUNC( GetAudioPlayerMode )
			ADD_ISACT_FUNC( SetAudioPlayerPosition )
			ADD_ISACT_FUNC( GetAudioPlayerPosition )

			ADD_ISACT_FUNC( SetListenerPosition )
			ADD_ISACT_FUNC( GetListenerPosition )
			ADD_ISACT_FUNC( SetListenerOrientation )

		LUA->SetField( -2, "ISACT" );
	LUA->Pop();


	return 0;
}

GMOD_MODULE_CLOSE()
{
	LUA_PRINT( "ISACT shutdown" );
	ShutdownISACT();
	return 0;
}