gmcl_isact-support
================

Unfinished library that adds incredibly basic and not very good Creative Labs ISACT support to Garry's Mod w/ Lua bindings. Proof of concept.

Requires ISACT binaries (isactwin.dll, AudioDrv.dll) and OpenAL (preferably, OpenAL-soft) in your Garry's Mod bin folder to work. Loads files relative to the executable dir.