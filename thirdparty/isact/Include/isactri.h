/*
*/
#ifndef _ISACTRESH_
#define _ISACTRESH_




///////////////////////////////////////////////////////////
// GUID definition

#ifndef GUID_DEFINED
    #define GUID_DEFINED
    typedef struct _GUID
    {
        unsigned long Data1;
        unsigned short Data2;
        unsigned short Data3;
        unsigned char Data4[8];
    } GUID;
#endif // GUID_DEFINED

#ifndef DEFINE_GUID
    #ifndef INITGUID
        #define DEFINE_GUID(name, l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8) \
                extern const GUID name
    #else
        #define DEFINE_GUID(name, l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8) \
                extern const GUID name = { l, w1, w2, { b1, b2,  b3,  b4,  b5,  b6,  b7,  b8 } }
    #endif // INITGUID
#endif // DEFINE_GUID
///////////////////////////////////////////////////////////




///////////////////////////////////////////////////////////
// Constants.

#define ISACT_MAX_VOICES	256

#define ISACT_MAX_SENDS 32
#define ISACT_MAX_NAME_LENGTH 128

// Default cone parameter values.
#define ISACT_DEFAULT_CONE_INSIDE_ANGLE    360
#define ISACT_DEFAULT_CONE_OUTSIDE_ANGLE   360
#define ISACT_DEFAULT_CONE_OUTSIDE_LEVEL     0
#define ISACT_DEFAULT_CONE_OUTSIDEHF_LEVEL   0

#define ISACT_MIN_DISTANCE_LOW  0
#define ISACT_MIN_DISTANCE_HIGH 65536
#define ISACT_MAX_DISTANCE_LOW 0
#define ISACT_MAX_DISTANCE_HIGH 65536
#define ISACT_MIN_DISTANCE_LEVEL_LOW (-100)
#define ISACT_MAX_DISTANCE_LEVEL_HIGH 0

#define ISACT_DEFAULT_MIN_DISTANCE 64
#define ISACT_DEFAULT_MAX_DISTANCE 2048
#define ISACT_DEFAULT_MAX_LEVEL (-30)

#define ISACT_DEFAULT_DISTANCE_MODIFIER 1
#define ISACT_MIN_DISTANCE_MODIFIER 0
#define ISACT_MAX_DISTANCE_MODIFIER 100

#define ISACT_DISTANCE_SEND_AUTO (1<<0)

#define ISACT_CONE_DIRECTHF_AUTO (1<<0)
#define ISACT_CONE_SENDHF_AUTO   (1<<1)

#define ISACT_MAX_EFFECT_PARAMETERS 64

#define ISACT_NULL_BUFFER 0xffffffff

///////////////////////////////////////////////////////////




///////////////////////////////////////////////////////////
// Data types.

typedef void (*SchedulerTimerProc)(void);

typedef enum
{
	COMPRESSION_FORMAT_PCM = 0,
	COMPRESSION_FORMAT_IMA4ADPCM = 1,
	COMPRESSION_FORMAT_OGGVORBIS = 2,
	COMPRESSION_FORMAT_WMA = 3,
	COMPRESSION_FORMAT_XMA = 4
} COMPRESSIONFORMAT;

///////////////////////////////////////////////////////////




///////////////////////////////////////////////////////////
// Structure definitions.

struct ISACTSampleInfo {
    unsigned long  ulBufferOffset,
                   ulTimeLength,
                   ulSamplesPerSecond,
                   ulByteLength;
    unsigned short usBitsPerSample;
};

struct ISACTDistanceData {
    float fDistanceSize,
          fDistanceLevel,
          fDistanceModifier;
    unsigned long ulDistanceFlags;
};

struct ISACTSoundDistance {
    float fMinDistance,
          fMaxDistance,
          fMaxDistanceLevel;
    unsigned long ulDistanceFlags;
};

struct ISACTSoundCone {
    long lInsideConeAngle,
         lOutsideConeAngle,
         lOutsideConeLevel,
         lOutsideConeHFLevel;
    unsigned long ulConeFlags;
};

struct ISACTMarker {
	long lSamplePosition;
	char szName[128];
};

struct ISACTVector {
    float fX,
          fY,
          fZ;
};

struct ISACTEuler {
    float flPitch,
          flYaw,
          flRoll;
};

struct ISACTTrackState {
    float fLevel,
          fDirect,
          fDirectHF,
          fPitch,
          fSend[ISACT_MAX_SENDS],
          fSendHF[ISACT_MAX_SENDS];
    unsigned long ulActiveSends,
				  ulMode;
    ISACTVector vPosition,
                vDirection,
                vVelocity,
                vPath;
};

struct ISACTEffectParameter {
    union {
        float fVector[3];
        int iValue;
        unsigned int uiValue;
        float fValue;
    };
};

struct ISACTObstruction
{
	long lObstruction;
	float flObstructionLFRatio;
};

struct ISACTOcclusion
{
	long lOcclusion;
	float flOcclusionLFRatio;
	float flOcclusionRoomRatio;
	float flOcclusionDirectRatio;
};

struct ISACTExclusion
{
	long lExclusion;
	float flExclusionLFRatio;
};

struct ISACTSlotState {
    float			fLevel;
    GUID			guidEffect;
    unsigned long	ulSlotFlags;
};
///////////////////////////////////////////////////////////




///////////////////////////////////////////////////////////
// Macros.

#define MAKE_EFFECT_PARAMETER(index, subindex) ((index & 0x3F)|((subindex & 0x03)<<6))
///////////////////////////////////////////////////////////




///////////////////////////////////////////////////////////
// Class definitions.

//
class ISACTRenderInterface
{
private:
    // Private data members.

protected:
    // Protected data members.

public:
    // Public data members.

private:
    // Private member functions.

protected:
    // Protected member functions.
    
public:
    virtual unsigned long SetSchedulerTimerProc(SchedulerTimerProc stp) = 0;
    virtual void BeginExclusiveMode(void) = 0;
    virtual void EndExclusiveMode(void) = 0;

    virtual void SendMidiDataStream(unsigned char *pDataStream, unsigned long ulDataStreamSize) = 0;

    virtual void LockProcessing(void) = 0;
    virtual void UnlockProcessing(void) = 0;

    virtual unsigned long GetTotalTrackCount(void) = 0;
    virtual unsigned long GetTotalSlotCount(void) = 0;

    virtual void ReleaseInterface(void) = 0;

    virtual bool LoadAudioSample(unsigned long ulChannels,
                                 unsigned short usBitsPerSample,
                                 unsigned long ulSamplesPerSecond,
								 long lCompressionFormat,
                                 unsigned long ulByteLength,
                                 const void *pData,
								 const void *pDataDesc,
                                 void **phOutHandle,
								 bool bRelease = false) = 0;
    virtual bool LoadAudioStreamingSample(unsigned long ulChannels,
                                          unsigned short usBitsPerSample,
                                          unsigned long ulSamplesPerSecond,
										  long lCompressionFormat,
                                          unsigned long ulPacketLength,
                                          const void *pData,
										  const void *pDataDesc,
                                          void **phOutHandle,
                                          bool bRelease) = 0;
    virtual void FreeAudioSample(void *hHandle) = 0;
    virtual void AttachSample(long lTrackIndex, 
                              void *hHandle, 
                              ISACTSoundDistance *pSoundDistance, 
                              ISACTSoundCone *pSoundCone) = 0;

    virtual unsigned long GetTrackProcessedStreamBufferCount(long lTrackIndex) = 0;
    virtual bool UpdateTrackStreamBuffer(long lTrackIndex, void *pData, unsigned long ulDataSize, void *pDataDesc = 0) = 0;
    
    virtual void PlayTracks(long *plTrackIndex, long lTrackCount, unsigned long ulLoopCount = 0) = 0;
    virtual void PauseTracks(long *plTrackIndex, long lTrackCount) = 0; 
    virtual void StopTracks(long *plTrackIndex, long lTrackCount) = 0; 
    virtual void StopAllTracks(void) = 0; 
    virtual void StopTracksLooping(long *plTrackIndex, long lTrackCount) = 0;

    virtual void SetTrackLevel(long lTrackIndex, float fLevel) = 0;
    virtual void SetTrackPosition(long lTrackIndex, ISACTVector &vPosition) = 0;
    virtual void SetTrackVelocity(long lTrackIndex, ISACTVector &vVelocity) = 0;
    virtual void SetTrackOrientation(long lTrackIndex, ISACTVector &vOrientation) = 0;
	virtual void SetTrackMode(long lTrackIndex, unsigned long ulMode) = 0;
	virtual void SetTrackPitch(long lTrackIndex, float fPitch) = 0;

    virtual void GetTrackState(long lTrackIndex, ISACTTrackState *pTrackState) = 0;
    virtual bool GetTrackPlayingState(long lTrackIndex) = 0;
    virtual ISACTVector GetTrackPosition(long lTrackIndex) = 0;

	virtual void SetTrackDirectLevel(long lTrackIndex, long lDirect, unsigned long ulDefer = 0) = 0;
    virtual void SetTrackDirectHFLevel(long lTrackIndex, long lDirectHF, unsigned long ulDefer = 0) = 0;
	virtual void SetTrackActiveSends(long lTrackIndex, unsigned long ulSendFlags, unsigned long ulDefer = 0) = 0;
	virtual void SetTrackSendLevel(long lTrackIndex, long lSendIndex, long lSend, unsigned long ulDefer = 0) = 0;
    virtual void SetTrackSendHFLevel(long lTrackIndex, long lSendIndex, long lSendHF, unsigned long ulDefer = 0) = 0;

    virtual void SetSlotLevel(long lSlotIndex, float fLevel, unsigned long ulDefer = 0) = 0;
    virtual void SetSlotFlags(long lSlotIndex, unsigned long ulSlotFlags, unsigned long ulDefer = 0) = 0;
    virtual void SetSlotEffect(long lSlotIndex, GUID guidEffect) = 0;
    virtual void SetSlotEffectParameter(long lSlotIndex, long lParameterIndex, void *pData, unsigned long ulDataSize, unsigned long ulDefer = 0) = 0;

	virtual void GetSlotLevel(long lSlotIndex, float *pfLevel) = 0;
    virtual void GetSlotFlags(long lSlotIndex, unsigned long *pulSlotFlags) = 0;
    virtual void GetSlotEffect(long lSlotIndex, GUID *pguidEffect) = 0;
    virtual void GetSlotEffectParameter(long lSlotIndex, long lParameterIndex, void *pData, unsigned long ulDataSize) = 0;

	virtual void SetTrackEAX(long lTrackIndex, unsigned long ulProperty, void *pData, unsigned long ulDataSize) = 0;
	virtual void GetTrackEAX(long lTrackIndex, unsigned long ulProperty, void *pData, unsigned long ulDataSize) = 0;

	virtual void SetSlotEAX(long lSlotIndex, unsigned long ulProperty, void *pData, unsigned long ulDataSize) = 0;
	virtual void GetSlotEAX(long lSlotIndex, unsigned long ulProperty, void *pData, unsigned long ulDataSize) = 0;

	virtual void SetContextEAX(unsigned long ulProperty, void *pData, unsigned long ulDataSize) = 0;
	virtual void GetContextEAX(unsigned long ulProperty, void *pData, unsigned long ulDataSize) = 0;

    virtual void SetListenerLevel(float fLevel) = 0;
	virtual void SetListenerPosition(ISACTVector &vPosition) = 0;
    virtual void SetListenerVelocity(ISACTVector &vVelocity) = 0;
    virtual void SetListenerOrientation(ISACTVector &vFront, ISACTVector &vUp) = 0;

	virtual void UpdateBufferDistanceData(void *hHandle, ISACTSoundDistance *pSoundDistance) = 0;
    virtual void UpdateBufferSoundCone(void *hHandle, ISACTSoundCone *pSoundCone) = 0;
	virtual void UpdateBufferDistanceData(long lTrackIndex, ISACTSoundDistance *pSoundDistance) = 0;
    virtual void UpdateBufferSoundCone(long lTrackIndex, ISACTSoundCone *pSoundCone) = 0;

	virtual bool QueryCompressionSupport(long lCompressionFormat) = 0;

/*	virtual void *ISACTMalloc(size_t lBytes, long lLineNum = 0, char *szSourceFile = 0) = 0;
	virtual void ISACTFree(void *pData) = 0;*/
};
///////////////////////////////////////////////////////////



///////////////////////////////////////////////////////////

#define EAX2SUPPORT		0x01
#define EAX3SUPPORT		0x02
#define EAX4SUPPORT		0x04

struct ISACTDeviceCaps
{
	char			szDeviceName[256];
	unsigned long	ulMaxVoices;
	unsigned long	ulEAXSupported;
	unsigned long	ulEAXEmulated;
};

extern "C"
{

// Get number of Open AL Devices on the system
unsigned long GetNumISACTDevices();

// Get the capabilities of a particular device
bool GetISACTDeviceCaps(unsigned long ulDeviceNum, ISACTDeviceCaps *pDeviceCaps);

// Initialize a particular enumerated AL Device and get the AL Context Pointer to
// pass to the RetrieveISACTRenderInterface
void *OpenISACTDevice(ISACTDeviceCaps *pDeviceCaps);

// Close a previously opened AL Device
void CloseISACTDevice(void *pData);

// Retrieve ISACTRenderInterface using Open AL Context pointer (from InitRenderDevice, or custom function),
// or NULL to allow the Renderer to create it's own Open AL Device + Context)
ISACTRenderInterface* RetrieveISACTRenderInterface(void *pUserData = 0, long lNumVoices = ISACT_MAX_VOICES);

}
///////////////////////////////////////////////////////////


#endif // _ISACTRESH_
