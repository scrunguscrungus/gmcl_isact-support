/*
*/
#ifndef _ISACTENGH_
#define _ISACTENGH_

#include "isactri.h"

#if (defined LINUX) || (defined MAC)
typedef long DWORD;
typedef void * HINSTANCE;
#endif


///////////////////////////////////////////////////////////
// Constants.

#ifndef NULL
#define NULL 0
#endif

#define LOOP_INFINITE -1
#define ISACT_SAC_SAMPLE_BANK 0xFFFFFFFF
#define ISACT_SAC_CONTENT_BANK	NULL
#define ISACT_SEARCH_CONTENT_BANKS	0xFFFFFFFF

#define ISACTPI 3.1415927f

#define ISACT_MIN_AZIMUTH 0
#define ISACT_MAX_AZIMUTH 360
#define ISACT_MIN_HEIGHT  (-90)
#define ISACT_MAX_HEIGHT  90
#define ISACT_MIN_ROLL    ISACT_MIN_AZIMUTH
#define ISACT_MAX_ROLL    ISACT_MAX_AZIMUTH

// Error Codes.
#define ISACT_OK                       0
#define ISACT_NOT_YET_FUNCTIONING    (-1)
#define ISACT_BAD_VALUE              (-2)
#define ISACT_MEMORY_ERROR           (-3)
#define ISACT_IN_TRANSITION          (-4)
#define ISACT_UNKNOWN_CONTENT	     (-5)
#define ISACT_BAD_CONTENT		     (-6)
#define ISACT_BAD_HANDLE		     (-7)
#define ISACT_FILE_OPEN_ERROR        (-8)
#define ISACT_NOT_INITIALIZED        (-9)
#define ISACT_NOT_FOUND             (-10)
#define ISACT_IN_USE                (-11)
#define ISACT_SAMPLEBANK_NOT_FOUND  (-12)
#define ISACT_CONTENTBANK_NOT_FOUND (-13)

///////////////////////////////////////////////////////////




///////////////////////////////////////////////////////////
// Data types.

typedef void (*IHANDLE);

typedef long ISACTRETURN;

///////////////////////////////////////////////////////////




///////////////////////////////////////////////////////////
// Structure definitions.

enum PLAYER_STATUS {PLAYER_STOPPED,
                    PLAYER_RELEASE,
                    PLAYER_PAUSED,
                    PLAYER_PLAYING,
                    PLAYER_PENDING_TRANSITION,
                    PLAYER_TRANSITIONING};

struct SPlayerStatus {
    PLAYER_STATUS PlayerStatus;
    IHANDLE       iHandlePrimaryContent,
                  iHandlePrimaryMarker,
                  iHandleTransitionContent,
                  iHandleTransitionMarker;
    long          lPrimaryLoopCount,
                  lTransitionLoopCount;
    unsigned long ulMilliseconds;
};

#define STATUS_FLAG_METRIC_CLOCK  (1<<0)
#define STATUS_FLAG_BEAT          (1<<1)
#define STATUS_FLAG_BAR           (1<<2)

#define STATUS_FLAG_TEMPO         (1<<3)
#define STATUS_FLAG_TIMESIGNATURE (1<<4)

struct SPlayerMetricTime {
    unsigned long ulMeasures,
                  ulBeats,
                  ulClocks,
                  ulTimeSignature,
                  ulTempo,
                  ulTotalClocks,
                  ulStatusFlags;
};

#define STATUS_FLAG_REAL_CLOCK    (1<<10)
#define STATUS_FLAG_FRAME         (1<<11)
#define STATUS_FLAG_SECOND        (1<<12)
#define STATUS_FLAG_MINUTE        (1<<13)
#define STATUS_FLAG_HOUR          (1<<14)

struct SPlayerRealTime {
    unsigned long ulHours,
                  ulMinutes,
                  ulSeconds,
                  ulFrames,
                  ulClocks,
                  ulTotalClocks,
                  ulStatusFlags;
};

#define STATUS_FLAG_MARKER        (1<<15)

enum TRANSITION_START {START_IMMEDIATE,
                       START_MARKER,
                       START_AFTER,
                       START_AFTER_LOOP};

struct STransition {
    TRANSITION_START StartType;
    float            fTransitionLength,
                     fPrimaryEndTime,
                     fPrimaryEndLevel,
                     fTransitionStartTime,
                     fTransitionStartLevel;
};

enum SYNC_START {SYNC_START_IMMEDIATE,
                 SYNC_START_CLOCK,
                 SYNC_START_BEAT,
                 SYNC_START_BAR,
                 SYNC_START_MARKER,
                 SYNC_START_COUNT};

struct SSyncStart {
    SYNC_START StartType;
    long       lMultiple;
};

struct SContentInfo {
	unsigned long ulMinDuration,
                  ulMaxDuration;
};

#define FORCE_SAMPLE_PRELOAD_FILE	0
#define FORCE_SAMPLE_PRELOAD_ON		1
#define FORCE_SAMPLE_PRELOAD_OFF	2

#define ISACT_ENABLE	0
#define ISACT_DISABLE	(-1)

///////////////////////////////////////////////////////////




///////////////////////////////////////////////////////////
// Macros.

#define FORCE_SAMPLE_BANK(a) (a|0x80000)
///////////////////////////////////////////////////////////




///////////////////////////////////////////////////////////
// Class definitions.

class ISACTInterface
{
public:
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	// ISACT API

	////////////////////////////////////////////////////////////////
	// File Functions
    virtual ISACTRETURN LoadFile(const unsigned short *szFileName) = 0;
    virtual ISACTRETURN LoadFile(const char *szFileName) = 0;
    virtual ISACTRETURN ReleaseAllResources(void) = 0;
    ////////////////////////////////////////////////////////////////




	////////////////////////////////////////////////////////////////
	// Sample Bank Functions
	virtual ISACTRETURN LoadSampleBankEx(unsigned long ulBankIndex, const unsigned short *szFileName, unsigned long ulFilePacketTimeLength = ISACT_ENABLE, long lPreload = FORCE_SAMPLE_PRELOAD_FILE, unsigned long ulRunTimeDecodePacketLength = ISACT_ENABLE) = 0;
    virtual ISACTRETURN LoadSampleBankEx(unsigned long ulBankIndex, const char *szFileName, unsigned long ulFilePacketTimeLength = ISACT_ENABLE, long lPreload = FORCE_SAMPLE_PRELOAD_FILE, unsigned long ulRunTimeDecodePacketLength = ISACT_ENABLE) = 0;
    virtual ISACTRETURN LoadSampleBank(unsigned long ulBankIndex, const unsigned short *szFileName, bool bEnableStreaming = true) = 0;
    virtual ISACTRETURN LoadSampleBank(unsigned long ulBankIndex, const char *szFileName, bool bEnableStreaming = true) = 0;
    virtual ISACTRETURN LoadStreamingSampleBank(unsigned long ulBankIndex, const unsigned short *szFileName, unsigned long ulPacketTimeLength, long lPreload = FORCE_SAMPLE_PRELOAD_FILE) = 0;
    virtual ISACTRETURN LoadStreamingSampleBank(unsigned long ulBankIndex, const char *szFileName, unsigned long ulPacketTimeLength, long lPreload = FORCE_SAMPLE_PRELOAD_FILE) = 0;
	virtual ISACTRETURN GetSampleBankIndex(unsigned long *pulBankIndex, const unsigned short *szSampleBankName) = 0;
	virtual ISACTRETURN GetSampleBankIndex(unsigned long *pulBankIndex, const char *szSampleBankName) = 0;
	virtual ISACTRETURN IsValidSampleBankIndex(unsigned long ulBankIndex) = 0;
	virtual ISACTRETURN UnloadSampleBank(unsigned long ulBankIndex) = 0;
	
    virtual ISACTRETURN GetSampleBankName(unsigned short *szSampleBankName, unsigned long ulBankIndex) = 0;
	virtual ISACTRETURN GetSampleBankName(char *szSampleBankName, unsigned long ulBankIndex) = 0;
	
    virtual ISACTRETURN GetSampleHandle(IHANDLE *piSampleHandle, const unsigned short *szSampleName, unsigned long ulBankIndex) = 0;
    virtual ISACTRETURN GetSampleHandle(IHANDLE *piSampleHandle, const char *szSampleName, unsigned long ulBankIndex) = 0;
    virtual ISACTRETURN GetSampleHandle(IHANDLE *piSampleHandle, unsigned long ulSampleIndex, unsigned long ulBankIndex) = 0;
	virtual ISACTRETURN IsValidSampleHandle(IHANDLE iHandleSample) = 0;
	virtual ISACTRETURN GetSampleGroupHandle(IHANDLE *piHandleGroup, IHANDLE iHandleSample) = 0;

    virtual ISACTRETURN GetSampleName(unsigned short *szSampleName, unsigned long ulSampleIndex, unsigned long ulBankIndex) = 0;
	virtual ISACTRETURN GetSampleName(char *szSampleName, unsigned long ulSampleIndex, unsigned long ulBankIndex) = 0;

    virtual ISACTRETURN UpdateStreamingSampleBanks(void) = 0;
    virtual ISACTRETURN CacheStreamingSample(unsigned short *szSampleName, unsigned long ulBankIndex) = 0;
    virtual ISACTRETURN CacheStreamingSample(char *szSampleName, unsigned long ulBankIndex) = 0;
    virtual ISACTRETURN CacheStreamingSample(unsigned long ulSampleIndex, unsigned long ulBankIndex) = 0;
    virtual ISACTRETURN CacheStreamingSample(IHANDLE iHandleSample) = 0;
	////////////////////////////////////////////////////////////////
	
    
    
    
	////////////////////////////////////////////////////////////////
	// Content Bank Functions
	virtual ISACTRETURN LoadContentBank(IHANDLE *piHandleContentBank, const unsigned short *szFileName) = 0;
	virtual ISACTRETURN LoadContentBank(IHANDLE *piHandleContentBank, const char *szFileName) = 0;
	virtual ISACTRETURN GetContentBankHandle(IHANDLE *piHandleContentBank, const unsigned short *szContentBankName) = 0;
	virtual ISACTRETURN GetContentBankHandle(IHANDLE *piHandleContentBank, const char *szContentBankName) = 0;
	virtual ISACTRETURN IsValidContentBankHandle(IHANDLE iHandleContentBank) = 0;
	virtual ISACTRETURN UnloadContentBank(IHANDLE iHandleContentBank) = 0;
	virtual ISACTRETURN SetContentBankSampleBankOffset(IHANDLE iHandleContentBank, long lSampleBankOffset) = 0;
	
	virtual ISACTRETURN GetNextContentBank(IHANDLE *piHandleContentBank, IHANDLE iHandleContentBank) = 0;
	virtual ISACTRETURN GetContentBankName(unsigned short *szContentBankName, IHANDLE iHandleContentBank) = 0;
	virtual ISACTRETURN GetContentBankName(char *szContentBankName, IHANDLE iHandleContentBank) = 0;

    virtual ISACTRETURN GetContentHandle(IHANDLE *piHandle, const unsigned short *szContentName, IHANDLE iHandleContentBank) = 0;
    virtual ISACTRETURN GetContentHandle(IHANDLE *piHandle, const char *szContentName, IHANDLE iHandleContentBank) = 0;
	virtual ISACTRETURN IsValidContentHandle(IHANDLE iHandleContent) = 0;
	virtual ISACTRETURN CacheContentSamples(IHANDLE iHandleContent, unsigned long ulReserved = 0) = 0;
	
	virtual ISACTRETURN GetNextContent(IHANDLE *piHandleContent, IHANDLE iHandleContent, IHANDLE iHandleContentBank) = 0;
	virtual ISACTRETURN GetContentName(unsigned short *szContentName, IHANDLE iHandleContent) = 0;
	virtual ISACTRETURN GetContentName(char *szContentName, IHANDLE iHandleContent) = 0;
	
    virtual ISACTRETURN GetContentVoiceCount(IHANDLE iHandleContent, long *plVoiceCount) = 0;
	virtual ISACTRETURN GetContentInfo(IHANDLE iHandleContent, SContentInfo *pContentInfo) = 0;
	virtual ISACTRETURN GetContentMarker(IHANDLE *piHandle, const unsigned short *szMarkerName, IHANDLE iHandleContent) = 0;
	virtual ISACTRETURN GetContentMarker(IHANDLE *piHandle, const char *szMarkerName, IHANDLE iHandleContent) = 0;
	virtual ISACTRETURN GetContentGroupHandle(IHANDLE *piHandleGroup, IHANDLE iHandleContent) = 0;
	virtual ISACTRETURN GetContentMaxGain(IHANDLE iHandleContent, ISACTVector *pvPosition, unsigned long ulHeadRelativeMode, float *pflGain) = 0;
	////////////////////////////////////////////////////////////////
	
    
    
    
    ////////////////////////////////////////////////////////////////
	// Base Functions
    virtual void ReleaseInterface(void) = 0;
	
    virtual ISACTRETURN GetAvailableVoiceCount(long *plCount) = 0;

    virtual ISACTRETURN StopAllAudio(IHANDLE iHandleGroup) = 0;
    virtual ISACTRETURN PauseAllAudio(IHANDLE iHandleGroup) = 0;
	virtual ISACTRETURN RestartAllAudio(IHANDLE iHandleGroup) = 0;
	
	virtual ISACTRETURN SetDistanceFactor(float flDistanceFactor) = 0;
	virtual ISACTRETURN GetDistanceFactor(float *pflDistanceFactor) = 0;

	virtual ISACTRETURN SetHeadRoomGain(float flGain) = 0;
	virtual ISACTRETURN GetHeadRoomGain(float *pflGain) = 0;

    virtual ISACTRETURN SetListenerGain(float fGain) = 0;
    virtual ISACTRETURN SetListenerPosition(ISACTVector &vPosition) = 0;
	virtual ISACTRETURN SetListenerOrientation(ISACTVector &vFront, ISACTVector &vUp) = 0;
	virtual ISACTRETURN SetListenerOrientation(float fAzimuth, float fHeight, float fRoll) = 0;

    virtual ISACTRETURN GetListenerPosition(ISACTVector *pvPosition) = 0;
	virtual ISACTRETURN GetListenerOrientation(ISACTVector *pvFront, ISACTVector *pvUp) = 0;
	virtual ISACTRETURN GetListenerOrientation(float *pfAzimuth, float *pfHeight, float *pfRoll) = 0;

	virtual ISACTRETURN SetEAXProperty(IHANDLE iHandle, const GUID &Guid, unsigned long ulProperty, void *pData, unsigned long ulDataSize) = 0;
	virtual ISACTRETURN GetEAXProperty(IHANDLE iHandle, const GUID &Guid, unsigned long ulProperty, void *pData, unsigned long ulDataSize) = 0;
	////////////////////////////////////////////////////////////////




	////////////////////////////////////////////////////////////////
    // Audio Player Functions
	virtual ISACTRETURN CreateAudioPlayer(IHANDLE *piHandle) = 0;
	virtual ISACTRETURN DestroyAudioPlayer(IHANDLE iHandlePlayer) = 0;
	virtual ISACTRETURN IsValidPlayerHandle(IHANDLE iHandle) = 0;
	
	virtual ISACTRETURN SetMasterAudioPlayer(IHANDLE iHandlePlayer) = 0;
	virtual ISACTRETURN GetMasterAudioPlayer(IHANDLE *piHandlePlayer) = 0;

    virtual ISACTRETURN AttachPlayerRPC(IHANDLE iHandlePlayer, const unsigned short *szRPCName, IHANDLE iHandleContentBank, float fRPCLevel) = 0;
    virtual ISACTRETURN AttachPlayerRPC(IHANDLE iHandlePlayer, const char *szRPCName, IHANDLE iHandleContentBank, float fRPCLevel) = 0;
    virtual ISACTRETURN ReleasePlayerRPC(IHANDLE iHandlePlayer) = 0;

    virtual ISACTRETURN AttachPlayerPrimaryContent(IHANDLE iHandlePlayer, IHANDLE iHandleContent) = 0;
	virtual ISACTRETURN AttachPlayerTransitionContent(IHANDLE iHandlePlayer, IHANDLE iHandleContent) = 0;
    virtual ISACTRETURN ReleasePlayerPrimaryContent(IHANDLE iHandlePlayer) = 0;
	virtual ISACTRETURN ReleasePlayerTransitionContent(IHANDLE iHandlePlayer) = 0;
	
    virtual ISACTRETURN PlayAudioPlayer(IHANDLE iHandlePlayer, long lLoopCount = 0, IHANDLE iHandleMarker = NULL) = 0;
	virtual ISACTRETURN TransitionAudioPlayer(IHANDLE iHandlePlayer, const unsigned short *szTransitionName, IHANDLE iHandleContentBank, long lLoopCount = 0, IHANDLE iHandleMarker = NULL) = 0;
	virtual ISACTRETURN TransitionAudioPlayer(IHANDLE iHandlePlayer, const char *szTransitionName, IHANDLE iHandleContentBank, long lLoopCount = 0, IHANDLE iHandleMarker = NULL) = 0;
	virtual ISACTRETURN TransitionAudioPlayer(IHANDLE iHandlePlayer, STransition *pTransition, long lLoopCount = 0, IHANDLE iHandleMarker = NULL) = 0;
    virtual ISACTRETURN PauseAudioPlayer(IHANDLE iHandlePlayer) = 0;
	virtual ISACTRETURN RestartAudioPlayer(IHANDLE iHandlePlayer) = 0;
	virtual ISACTRETURN StopAudioPlayer(IHANDLE iHandlePlayer) = 0;
	
    virtual ISACTRETURN GetAudioPlayerStatus(IHANDLE iHandlePlayer, SPlayerStatus *pPlayerStatus) = 0;
    virtual ISACTRETURN GetAudioPlayerMetricTime(IHANDLE iHandlePlayer, SPlayerMetricTime *pPlayerMetricTime) = 0;
    virtual ISACTRETURN GetAudioPlayerRealTime(IHANDLE iHandlePlayer, SPlayerRealTime *pPlayerRealTime) = 0;
	virtual ISACTRETURN GetAudioPlayerMaxGain(IHANDLE iHandlePlayer, float *pflGain) = 0;
    
    virtual ISACTRETURN SetAudioPlayerGain(IHANDLE iHandlePlayer, float flGain) = 0;
    virtual ISACTRETURN SetAudioPlayerPitch(IHANDLE iHandlePlayer, float flPitch) = 0;
    virtual ISACTRETURN SetAudioPlayerRPCLevel(IHANDLE iHandlePlayer, float flLevel) = 0;
    virtual ISACTRETURN SetAudioPlayerMode(IHANDLE iHandlePlayer, unsigned long ulMode) = 0;
    virtual ISACTRETURN SetAudioPlayerPosition(IHANDLE iHandlePlayer, ISACTVector &vPosition) = 0;
	virtual ISACTRETURN SetAudioPlayerOrientation(IHANDLE iHandlePlayer, ISACTVector &vFront, ISACTVector &vUp) = 0;
	virtual ISACTRETURN SetAudioPlayerOrientation(IHANDLE iHandlePlayer, float flAzimuth, float flHeight, float flRoll = 0) = 0;
	virtual ISACTRETURN SetAudioPlayerVelocity(IHANDLE iHandlePlayer, ISACTVector &vVelocity) = 0;
    
    virtual ISACTRETURN GetAudioPlayerGain(IHANDLE iHandlePlayer, float *pflGain) = 0;
    virtual ISACTRETURN GetAudioPlayerPitch(IHANDLE iHandlePlayer, float *pflPitch) = 0;
	virtual ISACTRETURN GetAudioPlayerRPCLevel(IHANDLE iHandlePlayer, float *pflLevel) = 0;
	virtual ISACTRETURN GetAudioPlayerMode(IHANDLE iHandlePlayer, unsigned long *pulMode) = 0;
	virtual ISACTRETURN GetAudioPlayerPosition(IHANDLE iHandlePlayer, ISACTVector *pvPosition) = 0;
	virtual ISACTRETURN GetAudioPlayerOrientation(IHANDLE iHandlePlayer, ISACTVector *pvFront, ISACTVector *pvUp) = 0;
	virtual ISACTRETURN GetAudioPlayerOrientation(IHANDLE iHandlePlayer, float *pflAzimuth = NULL, float *pflHeight = NULL, float *pflRoll = NULL) = 0;
	virtual ISACTRETURN GetAudioPlayerVelocity(IHANDLE iHandlePlayer, ISACTVector *pvVelocity) = 0;
	////////////////////////////////////////////////////////////////




	////////////////////////////////////////////////////////////////
	// Sound Entity Functions
	virtual ISACTRETURN CreateEntity(IHANDLE *piHandle, const unsigned short *szEntityName, IHANDLE iHandleContentBank) = 0;
	virtual ISACTRETURN CreateEntity(IHANDLE *piHandle, const char *szEntityName, IHANDLE iHandleContentBank) = 0;
	virtual ISACTRETURN DestroyEntity(IHANDLE iHandle) = 0;
	virtual ISACTRETURN IsValidEntityHandle(IHANDLE iHandle) = 0;
	virtual ISACTRETURN IsEntityPlaying(IHANDLE iHandle, bool *pbPlaying) = 0;
	virtual ISACTRETURN CacheEntitySamples(IHANDLE iHandle, unsigned long ulReserved = 0) = 0;
	
	virtual ISACTRETURN ActivateEntity(IHANDLE iHandle, bool bActivate) = 0;
	virtual ISACTRETURN ResetEntityLocalVars(IHANDLE iHandle) = 0;
	virtual ISACTRETURN ResetEntityGlobalVars(IHANDLE iHandleContentBank) = 0;
	virtual ISACTRETURN SuspendEntity(IHANDLE iHandle) = 0;
	virtual ISACTRETURN ProcessEntity(IHANDLE iHandle) = 0;
	virtual ISACTRETURN SuspendAllEntities(IHANDLE iHandleContentBank) = 0;
	virtual ISACTRETURN ProcessAllEntities(IHANDLE iHandleContentBank) = 0;

	virtual ISACTRETURN SetEntityLocalVarState(IHANDLE iHandle, const unsigned short *szVarName, const unsigned short *szStateName) = 0;
	virtual ISACTRETURN SetEntityLocalVarState(IHANDLE iHandle, const char *szVarName, const char *szStateName) = 0;
	virtual ISACTRETURN SetEntityGlobalVarState(IHANDLE iHandleContentBank, const unsigned short *szVarName, const unsigned short *szStateName) = 0;
	virtual ISACTRETURN SetEntityGlobalVarState(IHANDLE iHandleContentBank, const char *szVarName, const char *szStateName) = 0;
	virtual ISACTRETURN SetEntityLocalVarState(IHANDLE iHandle, unsigned long ulVariableIndex, unsigned long ulStateIndex) = 0;
	virtual ISACTRETURN SetEntityGlobalVarState(IHANDLE iHandleContentBank, unsigned long ulVariableIndex, unsigned long ulStateIndex) = 0;
	virtual ISACTRETURN SetEntityMasterRPCLevel(IHANDLE iHandle, float fLevel) = 0;
	virtual ISACTRETURN SetEntityRPCLevel(IHANDLE iHandle, const unsigned short *szRPCName, float fLevel) = 0;
	virtual ISACTRETURN SetEntityRPCLevel(IHANDLE iHandle, const char *szRPCName, float fLevel) = 0;
	virtual ISACTRETURN SetEntityMode(IHANDLE iHandle, unsigned long ulMode) = 0;
	virtual ISACTRETURN SetEntityPosition(IHANDLE iHandle, ISACTVector &vPosition) = 0;
	virtual ISACTRETURN SetEntityOrientation(IHANDLE iHandle, ISACTVector &vFront,ISACTVector &vUp) = 0;
	virtual ISACTRETURN SetEntityOrientation(IHANDLE iHandle, float flAzimuth, float flHeight, float flRoll = 0) = 0;
	virtual ISACTRETURN SetEntityVelocity(IHANDLE iHandle, ISACTVector &vVelocity) = 0;
	virtual ISACTRETURN SetEntityPitch(IHANDLE iHandle, float flPitch) = 0;
	virtual ISACTRETURN SetEntityGain(IHANDLE iHandle, float flGain) = 0;
	
	virtual ISACTRETURN GetEntityLocalVarState(IHANDLE iHandle, const unsigned short *szVarName, unsigned long *pulState) = 0;
	virtual ISACTRETURN GetEntityLocalVarState(IHANDLE iHandle, const char *szVarName, unsigned long *pulState) = 0;
	virtual ISACTRETURN GetEntityGlobalVarState(IHANDLE iHandleContentBank, const unsigned short *szVarName, unsigned long *pulState) = 0;
	virtual ISACTRETURN GetEntityGlobalVarState(IHANDLE iHandleContentBank, const char *szVarName, unsigned long *pulState) = 0;
	virtual ISACTRETURN GetEntityLocalVarState(IHANDLE iHandle, unsigned long ulVarIndex, unsigned long *pulStateIndex) = 0;
	virtual	ISACTRETURN GetEntityGlobalVarState(IHANDLE iHandleContentBank, unsigned long ulVarIndex, unsigned long *pulStateIndex) = 0;
	virtual ISACTRETURN GetEntityMasterRPCLevel(IHANDLE iHandle, float *pfLevel) = 0;
	virtual ISACTRETURN GetEntityRPCLevel(IHANDLE iHandle, const unsigned short *szRPCName, float *pfLevel) = 0;
	virtual ISACTRETURN GetEntityRPCLevel(IHANDLE iHandle, const char *szRPCName, float *pfLevel) = 0;
	virtual ISACTRETURN GetEntityMode(IHANDLE iHandle, unsigned long *pulMode) = 0;
	virtual ISACTRETURN GetEntityPosition(IHANDLE iHandle, ISACTVector *pvPosition) = 0;
	virtual ISACTRETURN GetEntityOrientation(IHANDLE iHandle, ISACTVector *pvFront, ISACTVector *pvUp) = 0;
	virtual ISACTRETURN GetEntityOrientation(IHANDLE iHandle, float *pflAzimuth, float *pflHeight, float *pflRoll) = 0;
	virtual ISACTRETURN GetEntityVelocity(IHANDLE iHandle, ISACTVector *pvVelocity) = 0;
	virtual ISACTRETURN GetEntityPitch(IHANDLE iHandle, float *pflPitch) = 0;
	virtual ISACTRETURN GetEntityGain(IHANDLE iHandle, float *pflGain) = 0;

	virtual ISACTRETURN GetEntityMaxGain(IHANDLE iHandle, ISACTVector *pvPosition, unsigned long ulHeadRelativeMode, float *pflGain) = 0;

	virtual ISACTRETURN GetNumLocalVars(IHANDLE iHandle, unsigned long *pulNumVars) = 0;
	virtual ISACTRETURN GetNumGlobalVars(IHANDLE iHandleContentBank, unsigned long *pulNumVars) = 0;
	virtual ISACTRETURN GetNumLocalVarStates(IHANDLE iHandle, unsigned long ulVarIndex, unsigned long *pulNumStates) = 0;
	virtual ISACTRETURN GetNumGlobalVarStates(IHANDLE iHandleContentBank, unsigned long ulVarIndex, unsigned long *pulNumStates) = 0;
	virtual ISACTRETURN GetLocalVarName(IHANDLE iHandle, unsigned long ulVarIndex, unsigned short *pszName, unsigned long ulNameLength) = 0;
	virtual ISACTRETURN GetLocalVarName(IHANDLE iHandle, unsigned long ulVarIndex, char *pszName, unsigned long ulNameLength) = 0;
	virtual ISACTRETURN GetGlobalVarName(IHANDLE iHandleContentBank, unsigned long ulVarIndex, unsigned short *pszName, unsigned long ulNameLength) = 0;
	virtual ISACTRETURN GetGlobalVarName(IHANDLE iHandleContentBank, unsigned long ulVarIndex, char *pszName, unsigned long ulNameLength) = 0;
	virtual ISACTRETURN GetLocalVarStateName(IHANDLE iHandle, unsigned long ulVarIndex, unsigned long ulStateIndex, unsigned short *pszName, unsigned long ulNameLength) = 0;
	virtual ISACTRETURN GetLocalVarStateName(IHANDLE iHandle, unsigned long ulVarIndex, unsigned long ulStateIndex, char *pszName, unsigned long ulNameLength) = 0;
	virtual ISACTRETURN GetGlobalVarStateName(IHANDLE iHandleContentBank, unsigned long ulVarIndex, unsigned long ulStateIndex, unsigned short *pszName, unsigned long ulNameLength) = 0;
	virtual ISACTRETURN GetGlobalVarStateName(IHANDLE iHandleContentBank, unsigned long ulVarIndex, unsigned long ulStateIndex, char *pszName, unsigned long ulNameLength) = 0;
	////////////////////////////////////////////////////////////////




	////////////////////////////////////////////////////////////////
	// Group Functions
	virtual ISACTRETURN GetGroupHandle(IHANDLE *piHandle, const unsigned short *szGroupName) = 0;
	virtual ISACTRETURN GetGroupHandle(IHANDLE *piHandle, const char *szGroupName) = 0;
	virtual ISACTRETURN IsValidGroupHandle(IHANDLE iHandle) = 0;

	virtual ISACTRETURN SetGroupGain(IHANDLE iHandle, float flGain, unsigned long ulFadeTime = 0) = 0;
	virtual ISACTRETURN GetGroupGain(IHANDLE iHandle, float *pflGain) = 0;
	////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
};
///////////////////////////////////////////////////////////




///////////////////////////////////////////////////////////
//

extern "C" ISACTInterface* RetrieveISACTInterface(ISACTRenderInterface *pRenderInterface);
///////////////////////////////////////////////////////////




#endif // _ISACTENGH_
